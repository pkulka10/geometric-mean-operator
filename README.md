The Geometric Mean Graph Operator provides a graph operator for creating a geometric mean graph from multiple graphs.

To run the operator:

1. Select two or more graph track labels while holding down the Shift key and selecting the appropriate track labels.  
2. Under the **Graph** tab, go to the **Operations** section.  
3. In the **Multi-Graph** dropdown menu, select the **Geometric Mean** option.
4. Select **Go** to the right of the dropdown menu.  A new graph track will be added with the geometric mean of the selected graphs.